from parse import tree
from functions import funcs
from util import isstring

var_list = {}

getvarlist = lambda : var_list

def evaluate(arr):
    if isinstance(arr, str) and arr not in funcs.keys() and not isstring(arr):
        if arr in var_list.keys():
            return var_list[arr]
        else:
            return arr
    elif isinstance(arr, list):
        woohoo = [evaluate(item) for item in arr]
        fn, *fn_args = woohoo
        if fn in funcs.keys():
            if fn == 'def':
                if len(fn_args) not in (1, 2):
                    raise Exception
                if len(fn_args) == 2:
                    return funcs[fn].getfn()(*fn_args, var_list)
                elif len(fn_args) == 1:
                    return funcs[fn].getfn()(*fn_args, 0, var_list)
            
            elif fn == '=':
                return funcs[fn].getfn()(arr[1], fn_args[1], var_list)
            
            elif fn == 'if':
                if len(fn_args) not in (2, 3):
                    raise Exception
                if len(fn_args) == 2:
                    return funcs[fn].getfn()[0](*fn_args)
                else:
                    return funcs[fn].getfn()[1](*fn_args)

            elif fn == 'read':
                if len(fn_args) not in (0, 1):
                    raise Exception
                if not len(fn_args):
                    return funcs[fn].getfn()()
                else:
                    return funcs[fn].getfn()(arr[1], var_list)

            elif fn == 'loop':
                if len(fn_args) != 2:
                    raise Exception
                return funcs[fn].getfn()(evaluate, arr[2], *fn_args)

            elif funcs[fn].getn() == 1:
                # UNARY
                if len(fn_args) == 1:
                    return funcs[fn].getfn()(*fn_args)
                else:
                    raise Exception

            elif funcs[fn].getn() == 2:
                # BINARY
                if len(fn_args) == 2:
                    return funcs[fn].getfn()(*fn_args)
                else:
                    raise Exception

            else:
                return funcs[fn].getfn()(*fn_args) 
    elif isstring(arr):
        return arr[1:-1]
    else:
        return arr